import React, { useState } from "react";
import "./App.css";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import MenuIcon from "@material-ui/icons/Menu";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import Slide from "@material-ui/core/Slide";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import { navStyles } from "./CssStyles";

function HideOnScroll(props) {
   const { children } = props;
   const trigger = useScrollTrigger();

   return (
      <Slide appear={false} direction="down" in={!trigger}>
         {children}
      </Slide>
   );
}

function Navigation(props) {
   const classes = navStyles();
   const [open, setOpen] = useState(false);

   const handleDrawerOpen = () => {
      setOpen(true);
   };

   const handleDrawerClose = () => {
      setOpen(false);
   };

   const navlist = [
      ["主頁🏠", "/"],
      ["角色🎎", "/characters"],
      ["武器🗡", "/weapons"],
      //["組隊", "/teams"]
   ];

   return (
      <div>
         <div className={classes.toolbar} />
         <HideOnScroll {...props}>
            <AppBar position="fixed" className={classes.root}>
               <Toolbar>
                  <IconButton
                     edge="start"
                     className={classes.menuButton}
                     color="inherit"
                     aria-label="menu"
                     onClick={handleDrawerOpen}
                     onMouseOver={handleDrawerOpen}
                  >
                     <MenuIcon />
                  </IconButton>
                  <Typography variant="h6" className={classes.title} lang="zh">
                     World Flipper 攻略
                  </Typography>
               </Toolbar>
            </AppBar>
         </HideOnScroll>
         <Drawer
            className={classes.drawer}
            open={open}
            PaperProps={{ onMouseLeave: handleDrawerClose }}
         >
            <List className={classes.list}>
               {navlist.map((obj, index) => (
                  <ListItem
                     button
                     key={obj[0]}
                     component="a"
                     href={obj[1]}
                     onClick={handleDrawerClose}
                  >
                     <ListItemText primary={obj[0]} lang="zh" />
                  </ListItem>
               ))}
            </List>
         </Drawer>
      </div>
   );
}

export default Navigation;
