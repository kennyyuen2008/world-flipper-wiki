import React, { useState } from "react";
import "./App.css";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import clsx from "clsx";
import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import CardActions from "@material-ui/core/CardActions";
import { ThemeProvider } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import { theme, panelStyles } from "./CssStyles";

function DetailPanel({ items, rarity, loading, type, detailHead, detailKey }) {
   const classes = panelStyles();
   const [expandedid, setExpandedid] = useState(null);

   const handleExpandClick = (id) => (e) => {
      setExpandedid(expandedid === id ? null : id);
      console.log(expandedid === id);
   };

   if (loading) {
      return null;
   }

   return (
      <ThemeProvider theme={theme}>
         <div className={classes.root}>
            {items
               .filter((item) => type ? item.rarity === rarity && item.types === type : item.rarity === rarity)
               .map((item, index) => (
                  <div key={index}>
                     <Card className={classes.card}>
                        <CardActionArea
                           value={index}
                           onClick={handleExpandClick(index)}
                        >
                           <CardMedia
                              className={classes.media}
                              component="img"
                              src={`${item.image[0]}`}
                           />
                           <CardContent>
                              <Typography variant="h5" component="h2" lang="zh">
                                 {item.name}
                              </Typography>
                           </CardContent>
                        </CardActionArea>
                        <CardActions disableSpacing>
                           <IconButton
                              className={clsx(classes.expand, {
                                 [classes.expandOpen]:
                                    expandedid === index ? true : false,
                              })}
                              edge="end"
                              onClick={handleExpandClick(index)}
                              aria-expanded={
                                 expandedid === index ? true : false
                              }
                              aria-label="show more"
                           >
                              <ExpandMoreIcon />
                           </IconButton>
                        </CardActions>
                        <Collapse
                           in={expandedid === index ? true : false}
                           timeout="auto"
                           unmountOnExit
                        >
                           <CardContent lang="zh">
                              <div className={classes.detailContainer}>
                                 {detailHead[0].map((obj, index) => [
                                    <Box color="primary.dark">
                                       <Typography>{obj}</Typography>
                                    </Box>,
                                    <Typography>
                                       {item[detailKey[0][index]]}
                                    </Typography>,
                                 ])}
                              </div>
                              {detailHead[1].map((obj, index) => [
                                 <Box color="primary.dark">
                                    <Typography>{obj}</Typography>
                                 </Box>,
                                 <Typography paragraph>
                                    {item[detailKey[1][index]]}
                                 </Typography>,
                              ])}
                           </CardContent>
                        </Collapse>
                     </Card>
                  </div>
               ))}
         </div>
      </ThemeProvider>
   );
}

export default DetailPanel;
