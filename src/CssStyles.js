import { createMuiTheme } from "@material-ui/core/styles";
import { grey, orange } from "@material-ui/core/colors";
import { makeStyles } from "@material-ui/core/styles";

export const theme = createMuiTheme({
	palette: {
		primary: {
			main: orange[800],
			dark: orange[300]
		},
		secondary: {
			main: grey[800]
		},
		type: "dark"
	}
});

export const navStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1
	},
	menuButton: {
		marginRight: theme.spacing(2)
	},
	title: {
		flexGrow: 1
	},
	drawer: {
		width: 240,
		flexShrink: 0
	},
	list: {
		width: 240,
		backgroundColor: theme.palette.background.paper
	},
	toolbar: theme.mixins.toolbar
}));

export const homeStyles = makeStyles((theme) => ({
	root: {
		display: "grid",
		gridTemplateColumns: "1fr"
	},
	panel: {
		padding: theme.spacing(2, 3),
		marginTop: "20px"
	},
	imageContainer: {
		marginTop: "15px",
		display: "Grid",
		gridTemplateColumns: "repeat(auto-fit, minmax(300px, 1fr))",
		gridGap: "24px"
	}
}));

export const loadingStyles = makeStyles((theme) => ({
	backdrop: {
		zIndex: theme.zIndex.drawer + 1,
		color: "#fff"
	}
}));

export const pageStyles = makeStyles({
	root: {
		flexGrow: 1,
		width: "100%"
	},
	second: {
		marginTop: "20px"
	}
});

export const panelStyles = makeStyles((theme) => ({
	root: {
		display: "grid",
		gridTemplateColumns: "repeat(auto-fit, minmax(250px, 1fr))",
		gridGap: "24px"
	},
	card: {
		maxWidth: 345
	},
	media: {
		width: "100%",
		margin: "5px"
	},
	expand: {
		transform: "rotate(0deg)",
		marginLeft: "auto",
		transition: theme.transitions.create("transform", {
			duration: theme.transitions.duration.shortest
		})
	},
	expandOpen: {
		transform: "rotate(180deg)"
	},
	detailContainer: {
		display: "grid",
		gridTemplateColumns: "1fr 1fr",
		marginBottom: "10px"
	}
}));
