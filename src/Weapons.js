import React, { useEffect, useState } from "react";
import Loading from "./Loading";
import { makeStyles } from "@material-ui/core/styles";
import { fetchFromFirestore } from "./Services";
import DetailPanel from "./DetailPanel";

const useStyles = makeStyles({
	root: {
		flexGrow: 1,
		width: "100%"
	},
	second: {
		marginTop: "20px"
	}
});

function Weapons() {
	const classes = useStyles();
	const [items, setItems] = useState([]);
	const [loading, setLoading] = useState(true);

	useEffect(() => {
		fetchFromFirestore("weapons").then(data => {
			setItems(data);
			setLoading(false);
		});
	}, []);

	const rarities = [5];
	const detailHead = [
		["最初血量", "最大血量", "最初攻擊力", "最大攻擊力", "屬性"],
		["獲得來源", "最初效果", "最大效果"]
	];
	const detailKey = [
		["hp", "max_hp", "atk", "max_atk", "type"],
		["from", "skill", "max_skill"]
	];

	return (
		<div className={classes.root}>
			{rarities.map(rarity => [
				<Loading loading={loading} />,
				<DetailPanel
					items={items}
					rarity={rarity}
					loading={loading}
					detailHead={detailHead}
					detailKey={detailKey}
				/>
			])}
		</div>
	);
}

export default Weapons;
