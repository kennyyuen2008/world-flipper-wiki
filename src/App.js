import React from "react";
import "./App.css";
import "typeface-roboto";
import Navigation from "./Navigation";
import Characters from "./Characters";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Weapons from "./Weapons";
import Home from "./Home";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import { grey, orange } from "@material-ui/core/colors";
import Box from "@material-ui/core/Box";

const darkTheme = createMuiTheme({
   palette: {
      primary: {
         main: orange[700]
      },
      secondary: {
         main: grey[800]
      },
      type: "dark"
   }
});

function App() {
   return (
      <ThemeProvider theme={darkTheme}>
         <Box bgcolor="secondary.dark">
            <Navigation />
            <div className="App">
               <div className="container">
                  <Router>
                     <Switch>
                        <Route path="/weapons" component={Weapons} />
                        <Route path="/characters" component={Characters} />
                        <Route path="/" component={Home} />
                     </Switch>
                  </Router>
               </div>
            </div>
         </Box>
      </ThemeProvider>
   );
}

export default App;
