import React, { useEffect, useState } from "react";
import "./App.css";
import Typography from "@material-ui/core/Typography";
import { ThemeProvider } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import { Paper } from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import { fetchFromFirestore } from "./Services";
import { theme, homeStyles } from "./CssStyles";

function ListItemLink(props) {
   return <ListItem button component="a" {...props} />;
}

function Home() {
   const classes = homeStyles();

   const [events, setEvents] = useState([]);
   const [draws, setDraws] = useState([]);

   const currentTimestamp = Math.floor(Date.now() / 1000);

   useEffect(() => {
      fetchFromFirestore("events").then((data) => {
         setEvents(data);
      });
      fetchFromFirestore("draws").then((data) => {
         setDraws(data);
      });
   }, []);

   return (
      <ThemeProvider theme={theme}>
         <CssBaseline />
         <div className={classes.root}>
            <Box bgcolor="secondary.dark">
               <img
                  src={`${process.env.PUBLIC_URL}/image/banner.webp`}
                  className="banner"
                  alt="banner"
               />
               <div className={classes.container}>
                  <Paper square className={classes.panel}>
                     <Box color="primary.light">
                        <Typography lang="zh">開催中活動🎊</Typography>
                     </Box>
                     <Box
                        bgcolor="secondary.main"
                        className={classes.imageContainer}
                     >
                        {events
                           .filter(
                              (event) =>
                                 event.startDate.seconds < currentTimestamp &&
                                 event.endDate.seconds > currentTimestamp
                           )
                           .map((event) => (
                              <img
                                 key={event.image}
                                 src={`${process.env.PUBLIC_URL}/image/${event.image}`}
                                 className="eventimg"
                                 alt="event"
                              />
                           ))}
                     </Box>
                  </Paper>
                  <Paper square className={classes.panel}>
                     <Box color="primary.light">
                        <Typography lang="zh">開催中抽蛋🎁</Typography>
                     </Box>
                     <Box
                        bgcolor="secondary.main"
                        className={classes.imageContainer}
                     >
                        {draws
                           .filter(
                              (draw) =>
                                 draw.startDate.seconds < currentTimestamp &&
                                 draw.endDate.seconds > currentTimestamp
                           )
                           .map((draw) => (
                              <img
                                 key={draw.image}
                                 src={`${process.env.PUBLIC_URL}/image/${draw.image}`}
                                 className="eventimg"
                                 alt="getcha"
                              />
                           ))}
                     </Box>
                  </Paper>
                  <ExpansionPanel style={{ marginTop: "20px" }}>
                     <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel1a-content"
                     >
                        <Typography className={classes.heading} lang="zh">
                           過去活動🎊
                        </Typography>
                     </ExpansionPanelSummary>
                     <ExpansionPanelDetails>
                        <Box
                           bgcolor="secondary.main"
                           className={classes.imageContainer}
                        >
                           {events
                              .filter(
                                 (event) =>
                                    event.endDate.seconds < currentTimestamp &&
                                    event.endDate.seconds > 0
                              )
                              .map((event) => (
                                 <img
                                    key={event.image}
                                    src={`${process.env.PUBLIC_URL}/image/${event.image}`}
                                    className="eventimg"
                                    alt="event"
                                 />
                              ))}
                        </Box>
                     </ExpansionPanelDetails>
                  </ExpansionPanel>
                  <ExpansionPanel>
                     <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel1a-content"
                     >
                        <Typography className={classes.heading} lang="zh">
                           過去抽蛋🎁
                        </Typography>
                     </ExpansionPanelSummary>
                     <Paper square className={classes.panel}>
                        <Box
                           bgcolor="secondary.main"
                           className={classes.imageContainer}
                        >
                           {draws
                              .filter(
                                 (draw) =>
                                    draw.endDate.seconds < currentTimestamp &&
                                    draw.endDate.seconds > 0
                              )
                              .map((draw) => (
                                 <img
                                    key={draw.image}
                                    src={`${process.env.PUBLIC_URL}/image/${draw.image}`}
                                    className="eventimg"
                                    alt="event"
                                 />
                              ))}
                        </Box>
                     </Paper>
                  </ExpansionPanel>
                  <ExpansionPanel defaultExpanded>
                     <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel1a-content"
                     >
                        <Typography className={classes.heading} lang="zh">
                           網站簡介💎
                        </Typography>
                     </ExpansionPanelSummary>
                     <ExpansionPanelDetails>
                        <Typography component="p" lang="zh" paragraph>
                           World Flipper Unofficial Wiki.👩‍💻
                           彈珠世界攻略網，以回應式網頁設計🛠，提供一個方便、快速🚀的方式給玩家以跟隨最新活動🎊、角色🎎及武器🗡資料。
                           在所有裝置上📲都提供完整功能為目標。
                        </Typography>
                     </ExpansionPanelDetails>
                  </ExpansionPanel>
                  <ExpansionPanel defaultExpanded>
                     <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel1a-content"
                     >
                        <Typography className={classes.heading} lang="zh">
                           遊戲簡介🎮
                        </Typography>
                     </ExpansionPanelSummary>
                     <ExpansionPanelDetails>
                        <Typography component="p" lang="zh" paragraph>
                           《彈射世界》由 Cygames 子公司 Citail
                           進行開發的💩遊戲。 玩法則是以養成可愛😘角色系統 +
                           彈珠台作為遊戲賣點。實則以😈抽蛋去賺取課金💰。
                        </Typography>
                     </ExpansionPanelDetails>
                  </ExpansionPanel>
                  <ExpansionPanel>
                     <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel1a-content"
                     >
                        <Typography className={classes.heading} lang="zh">
                           注意事項⚠️
                        </Typography>
                     </ExpansionPanelSummary>
                     <ExpansionPanelDetails>
                        <List component="nav" aria-label="list" lang="zh">
                           <ListItem>
                              <ListItemText primary="本網站為非官方策略網⚔️。它不保證信息的有效性或準確性🔍，也不承擔任何責任。" />
                           </ListItem>
                           <ListItem>
                              <ListItemText primary="本網站不賠償任何使用時所造成的任何損害💔，用戶請自擔風險⚖️。" />
                           </ListItem>
                           <ListItem>
                              <ListItemText primary="本網站中使用的遊戲圖像🖼和信息📃的權利屬於Cygames🎮。" />
                           </ListItem>
                           <ListItemLink href="https://gitlab.com/kennyyuen2008/world-flipper-wiki">
                              <ListItemText primary="本網站為Open source🌎可按此到gitlab查看。" />
                           </ListItemLink>
                        </List>
                     </ExpansionPanelDetails>
                  </ExpansionPanel>
               </div>
            </Box>
         </div>
      </ThemeProvider>
   );
}

export default Home;
