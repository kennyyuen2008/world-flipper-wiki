import { firestore, storage } from "./Firebase";
export async function fetchFromFirestore(collection) {
   const temp = [];
   const snapshot = await firestore.collection(collection).get();

   if (collection === "events" || collection === "draws") {
      snapshot.docs.forEach((doc) => temp.push(doc.data()));
      return temp;
   }

   return Promise.all(
      snapshot.docs.map(async (doc) => {
         const data = doc.data();
         try {
            const url = await storage
               .ref()
               .child(`${data.image[0]}.png`)
               .getDownloadURL();

            data.image[0] = url;
         } catch (error) {
         }

         return data;
      })
   ).then((data) => {
      return data;
   });
}
export async function fetchFromRESTApi(collection) {
   const url = `https://worldflipper-5262.restdb.io/rest/${collection}`;
   fetch(url, {
      method: "GET",
      headers: {
         "x-apikey": "5dea05ea4658275ac9dc23a2",
      },
   })
      .then((data) => data.json())
      .then((data) => {
         return data;
      });
}
