import React, { useEffect, useState } from "react";
import Loading from "./Loading";
import "./App.css";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import TabPanel from "./TabPanel";
import { fetchFromFirestore, fetchImagesFromCloudStorage } from "./Services";
import { pageStyles } from "./CssStyles";
import DetailPanel from "./DetailPanel";

function Characters() {
   const classes = pageStyles();

   const [items, setItems] = useState({});
   const [type, setType] = useState("火");
   const [rarity, setRarity] = useState(5);
   const [loading, setLoading] = useState(true);

   useEffect(() => {
      fetchFromFirestore("characters").then((data) => {
         setItems(data);
         setLoading(false);
      });
   }, []);

   const handleTypeChange = (event, newType) => {
      setType(newType);
   };
   const handleRarityChange = (event, newRarity) => {
      setRarity(newRarity);
   };

   const rarities = [5, 4, 3];
   const types = ["火", "水", "風", "雷", "光", "暗"];
   const detailHead = [
      ["最大血量", "最大攻擊力", "聲優", "性別", "種族"],
      ["隊長技", "主動技", "被動 1", "被動 2", "被動 3"],
   ];
   const detailKey = [
      ["max_hp", "attack", "cv", "gender", "race"],
      ["leader_skill", "active_skill", "passive1", "passive2", "passive3"],
   ];

   return (
      <div className={classes.root}>
         <CssBaseline />
         <AppBar position="static" color="default">
            <Tabs
               value={type}
               indicatorColor="primary"
               variant="scrollable"
               scrollButtons="on"
               textColor="primary"
               onChange={handleTypeChange}
               aria-label="tabs"
               style={{ maxWidth: "100vw" }}
            >
               {types.map((type, index) => (
                  <Tab key={index} label={type} lang="zh" value={type} />
               ))}
            </Tabs>
         </AppBar>
         <AppBar position="static" color="default" className={classes.second}>
            <Tabs
               value={rarity}
               indicatorColor="primary"
               variant="scrollable"
               scrollButtons="on"
               textColor="primary"
               onChange={handleRarityChange}
               aria-label="tabs"
               style={{ maxWidth: "100vw" }}
            >
               {rarities.map((rarity, index) => (
                  <Tab
                     key={index}
                     label={`${rarity}星`}
                     lang="zh"
                     value={rarity}
                  />
               ))}
            </Tabs>
         </AppBar>
         {rarities.map((r, index) => (
            <TabPanel value={rarity} index={r} key={index}>
               <DetailPanel
                  key={index}
                  items={items}
                  rarity={r}
                  loading={loading}
                  type={type}
                  detailHead={detailHead}
                  detailKey={detailKey}
               />
            </TabPanel>
         ))}

         <Loading loading={loading} />
      </div>
   );
}

export default Characters;
