import React from "react";
import "./App.css";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import { loadingStyles } from "./CssStyles";

function Loading({ loading }) {
	const classes = loadingStyles();
	if (!loading) {
		return null;
	}

	return (
		<Backdrop className={classes.backdrop} open={loading}>
			<CircularProgress color="inherit" />
		</Backdrop>
	);
}

export default Loading;
