const firebase = require("firebase/app");
// Add the Firebase products that you want to use
require("firebase/auth");
require("firebase/firestore");
require("firebase/storage");
const config = require("./config");

const Firebase = firebase.initializeApp(config);

const firestore = Firebase.firestore();
const storage = Firebase.storage();

module.exports = { Firebase, firestore, storage };
